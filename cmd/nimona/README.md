## Usage

### Daemon

Start and configure a local peer.

```
nimona daemon init
nimona daemon start
```

### Client

Run commands against a daemon

```
nimona object
nimona help
```
