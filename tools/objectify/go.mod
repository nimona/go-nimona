module nimona.io/tools/objectify

go 1.12

require (
	github.com/mitchellh/mapstructure v1.1.2
	github.com/stretchr/testify v1.3.0
	golang.org/x/tools v0.0.0-20190713164153-8b927904ee0d
	nimona.io v0.3.1
)

replace nimona.io => ../../
