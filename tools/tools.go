package tools

import (
	// See README.md before touching this file
	_ "github.com/cheekybits/genny"
	_ "github.com/golangci/golangci-lint/cmd/golangci-lint"
	_ "github.com/goreleaser/goreleaser"
	_ "github.com/golangci/golangci-lint/cmd/golangci-lint"
	_ "github.com/shurcooL/vfsgen/cmd/vfsgendev"
	_ "github.com/vektra/mockery/cmd/mockery"
)
