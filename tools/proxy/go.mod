module nimona.io/tools/proxy

go 1.12

require (
	github.com/caarlos0/env/v6 v6.0.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	golang.org/x/net v0.0.0-20190713164153-da137c7871d7
)
