package dht

import (
	"context"
	"sync"
	"time"

	"nimona.io/internal/log"
	"nimona.io/pkg/crypto"
	"nimona.io/pkg/net/peer"
	"nimona.io/pkg/object"
)

const numPeersNear int = 15

type QueryType int

const (
	PeerInfoQuery QueryType = iota
	ProviderQuery
)

type query struct {
	dht                *DHT
	id                 string
	key                string
	queryType          QueryType
	closestFingerprint crypto.Fingerprint
	contactedPeers     sync.Map
	incomingPayloads   chan interface{}
	outgoingPayloads   chan interface{}
	logger             log.Logger
}

func (q *query) Run(ctx context.Context) {
	q.logger = log.FromContext(ctx)
	go func() {
		// send what we know about the key
		switch q.queryType {
		case PeerInfoQuery:
			if peerInfo, err := q.dht.peerStore.Get(q.key); err == nil {
				if peerInfo == nil {
					q.logger.Warn("got nil peerInfo", log.String("requestID", q.key))
					break
				}
				q.outgoingPayloads <- peerInfo
			}
		case ProviderQuery:
			if providers, err := q.dht.store.GetProviders(q.key); err == nil {
				for _, provider := range providers {
					q.outgoingPayloads <- provider
				}
			}
		}

		// and now, wait for something to happen
		for {
			select {
			case incPayload := <-q.incomingPayloads:
				switch payload := incPayload.(type) {
				case *peer.PeerInfo:
					q.outgoingPayloads <- payload
					// TODO next doesn't work
					// q.nextIfCloser(object.SenderPeerInfo.Metadata.Signer)
				case *Provider:
					// TODO check if id is in payload.ObjectIDs
					for _, objectID := range payload.ObjectIDs {
						if objectID == q.key {
							q.outgoingPayloads <- payload
							break
						}
					}
					// TODO next doesn't work
					// q.nextIfCloser(object.SenderPeerInfo.Metadata.Signer)
				}

			case <-time.After(maxQueryTime):
				close(q.outgoingPayloads)
				return

			case <-ctx.Done():
				close(q.outgoingPayloads)
				return
			}
		}
	}()

	// start looking for the node
	go q.next()
}

func (q *query) nextIfCloser(newFingerprint crypto.Fingerprint) {
	if q.closestFingerprint == "" {
		q.closestFingerprint = newFingerprint
		q.next()
	} else {
		// find closest peer
		closestPeers, err := q.dht.FindPeersClosestTo(q.key, 1)
		if err != nil {
			// TODO log error
			return
		}
		if len(closestPeers) == 0 {
			return
		}
		closestFingerprint := closestPeers[0].Fingerprint()
		if comparePeers(q.closestFingerprint, closestFingerprint, crypto.Fingerprint(q.key)) == closestFingerprint {
			q.closestFingerprint = closestFingerprint
			q.next()
		}
	}
}

func (q *query) next() {
	// find closest peers
	closestPeers, err := q.dht.FindPeersClosestTo(q.key, numPeersNear)
	if err != nil {
		q.logger.Warn("Failed find peers near", log.Error(err))
		return
	}

	peersToAsk := []*crypto.PublicKey{}
	for _, peerInfo := range closestPeers {
		// skip the ones we've already asked
		if _, ok := q.contactedPeers.Load(peerInfo.Fingerprint()); ok {
			continue
		}
		peersToAsk = append(peersToAsk, peerInfo.Signature.PublicKey)
		q.contactedPeers.Store(peerInfo.Fingerprint(), true)
	}

	signer := q.dht.key

	var o *object.Object
	switch q.queryType {
	case PeerInfoQuery:
		req := &PeerInfoRequest{
			RequestID:   q.id,
			Fingerprint: crypto.Fingerprint(q.key),
		}
		o = req.ToObject()
		err = crypto.Sign(o, signer)
	case ProviderQuery:
		req := &ProviderRequest{
			RequestID: q.id,
			Key:       q.key,
		}
		o = req.ToObject()
		err = crypto.Sign(o, signer)
	default:
		return
	}

	if err != nil {
		return
	}

	ctx := context.Background()
	logger := log.FromContext(ctx)
	for _, peer := range peersToAsk {
		addr := "peer:" + peer.Fingerprint().String()
		if err := q.dht.exchange.Send(ctx, o, addr); err != nil {
			logger.Warn("query next could not send", log.Error(err), log.String("fingerprint", peer.Fingerprint().String()))
		}
	}
}
