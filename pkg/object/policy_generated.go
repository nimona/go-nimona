// Code generated by nimona.io/tools/objectify. DO NOT EDIT.

// +build !generate

package object

import (
	"github.com/mitchellh/mapstructure"
)

const (
	PolicyType = "/policy"
)

// ToObject returns a f12n object
func (s Policy) ToObject() *Object {
	o := New()
	o.SetType(PolicyType)
	if s.Description != "" {
		o.SetRaw("description", s.Description)
	}
	if len(s.Subjects) > 0 {
		o.SetRaw("subjects", s.Subjects)
	}
	if len(s.Actions) > 0 {
		o.SetRaw("actions", s.Actions)
	}
	if s.Effect != "" {
		o.SetRaw("effect", s.Effect)
	}
	return o
}

func anythingToAnythingForPolicy(
	from interface{},
	to interface{},
) error {
	config := &mapstructure.DecoderConfig{
		Result:  to,
		TagName: "json",
	}

	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return err
	}

	if err := decoder.Decode(from); err != nil {
		return err
	}

	return nil
}

// FromObject populates the struct from a f12n object
func (s *Policy) FromObject(o *Object) error {
	atoa := anythingToAnythingForPolicy
	if err := atoa(o.GetRaw("description"), &s.Description); err != nil {
		return err
	}
	if err := atoa(o.GetRaw("subjects"), &s.Subjects); err != nil {
		return err
	}
	if err := atoa(o.GetRaw("actions"), &s.Actions); err != nil {
		return err
	}
	if err := atoa(o.GetRaw("effect"), &s.Effect); err != nil {
		return err
	}

	if ao, ok := interface{}(s).(interface{ afterFromObject() }); ok {
		ao.afterFromObject()
	}

	return nil
}

// GetType returns the object's type
func (s Policy) GetType() string {
	return PolicyType
}
