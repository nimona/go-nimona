package dag

import (
	"nimona.io/pkg/object"
)

type Graph struct {
	Objects []*object.Object
}
