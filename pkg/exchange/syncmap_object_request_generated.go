// This file was automatically generated by genny.
// Any changes will be lost if this file is regenerated.
// see https://github.com/cheekybits/genny

package exchange

import "sync"

type (
	// StringObjectRequestSyncMap -
	StringObjectRequestSyncMap struct {
		m sync.Map
	}
)

// NewStringObjectRequestSyncMap constructs a new SyncMap
func NewStringObjectRequestSyncMap() *StringObjectRequestSyncMap {
	return &StringObjectRequestSyncMap{}
}

// Put -
func (m *StringObjectRequestSyncMap) Put(k string, v *ObjectRequest) {
	m.m.Store(k, v)
}

// Get -
func (m *StringObjectRequestSyncMap) Get(k string) (*ObjectRequest, bool) {
	i, ok := m.m.Load(k)
	if !ok {
		return nil, false
	}

	v, ok := i.(*ObjectRequest)
	if !ok {
		return nil, false
	}

	return v, true
}

// Delete -
func (m *StringObjectRequestSyncMap) Delete(k string) {
	m.m.Delete(k)
}
