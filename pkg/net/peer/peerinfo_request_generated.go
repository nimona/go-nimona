// Code generated by nimona.io/tools/objectify. DO NOT EDIT.

// +build !generate

package peer

import (
	"github.com/mitchellh/mapstructure"
	"nimona.io/pkg/object"
)

const (
	PeerInfoRequestType = "/peer.request"
)

// ToObject returns a f12n object
func (s PeerInfoRequest) ToObject() *object.Object {
	o := object.New()
	o.SetType(PeerInfoRequestType)
	if len(s.Keys) > 0 {
		o.SetRaw("keys", s.Keys)
	}
	if len(s.Protocols) > 0 {
		o.SetRaw("protocols", s.Protocols)
	}
	if len(s.ContentIDs) > 0 {
		o.SetRaw("contentIDs", s.ContentIDs)
	}
	if len(s.ContentTypes) > 0 {
		o.SetRaw("contentTypes", s.ContentTypes)
	}
	return o
}

func anythingToAnythingForPeerInfoRequest(
	from interface{},
	to interface{},
) error {
	config := &mapstructure.DecoderConfig{
		Result:  to,
		TagName: "json",
	}

	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return err
	}

	if err := decoder.Decode(from); err != nil {
		return err
	}

	return nil
}

// FromObject populates the struct from a f12n object
func (s *PeerInfoRequest) FromObject(o *object.Object) error {
	atoa := anythingToAnythingForPeerInfoRequest
	if err := atoa(o.GetRaw("keys"), &s.Keys); err != nil {
		return err
	}
	if err := atoa(o.GetRaw("protocols"), &s.Protocols); err != nil {
		return err
	}
	if err := atoa(o.GetRaw("contentIDs"), &s.ContentIDs); err != nil {
		return err
	}
	if err := atoa(o.GetRaw("contentTypes"), &s.ContentTypes); err != nil {
		return err
	}

	if ao, ok := interface{}(s).(interface{ afterFromObject() }); ok {
		ao.afterFromObject()
	}

	return nil
}

// GetType returns the object's type
func (s PeerInfoRequest) GetType() string {
	return PeerInfoRequestType
}
