package peer

import (
	"nimona.io/pkg/crypto"
)

//go:generate $GOBIN/objectify -schema /peer.request -type PeerInfoRequest -in peerinfo_request.go -out peerinfo_request_generated.go

// PeerInfoRequest is a request for a peer info
type PeerInfoRequest struct {
	Keys         []crypto.Fingerprint
	Protocols    []string
	ContentIDs   []string
	ContentTypes []string
}
